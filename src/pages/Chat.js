import {useEffect, useState} from "react";
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import SockJsClient from 'react-stomp';

const Chat = () => {
    const [username, setUsername] = useState('');
    const [message, setMessage] = useState('');
    const [open, setOpen] = useState(false);
    const [clientRef, setClientRef] = useState('');
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const user = localStorage.getItem("username");
        if (user) {
            setUsername(user)
            setOpen(false)
        } else {
            setOpen(true)
        }
    }, [])

    const sendMessage = () => {
        if (message)
            clientRef.sendMessage('/app/user-all', JSON.stringify({
                name: username,
                message: message
            }));
    };

    return (
        <div>
            <Row>
                <Col md={{size: 4, offset: 5}}>
                    <h1 className="text-center">Welcome to websocket</h1>
                    <h4 className="text-center">User: {username}</h4>
                    <Row>
                        <Col md={{size: 9}}>
                            <Input
                                onChange={(e) => setMessage(e.target.value)}
                                placeholder="Enter text to send"/>
                        </Col>
                        <Col>
                            <Button color="success"
                                    onClick={sendMessage}
                            >Send</Button>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col md={{offset: 1}}>
                            {messages.map(msg =>
                                <p>{msg.name} : {msg.message}</p>
                            )}
                        </Col>
                    </Row>
                </Col>
            </Row>


            <Modal isOpen={open}>
                <ModalHeader>Chat</ModalHeader>
                <ModalBody>
                    <Input
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Enter username"/>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => {
                        if (username) {
                            setOpen(false)
                            localStorage.setItem("username", username)
                        }
                    }}>Save</Button>
                </ModalFooter>
            </Modal>
            <SockJsClient
                url='http://localhost:8080/websocket-chat/'
                topics={['/topic/user']}
                onConnect={() => {
                    console.log("connected");
                }}
                onDisconnect={() => {
                    console.log("Disconnected");
                }}
                onMessage={(msg) => {
                    setMessages([...messages, msg])
                }}
                ref={(client) => {
                    setClientRef(client);
                }}/>
        </div>
    )
}

export default Chat;
import SocialLoginComponent from "../components/SocialLoginComponent";
import {
    FACEBOOK_CONNECT_URL,
    FACEBOOK_SIGN_URL, GITHUB_CONNECT_URL,
    GITHUB_SIGN_URL,
    GOOGLE_CONNECT_URL,
    GOOGLE_SIGN_URL
} from "../utils/api";
import {ACCESS_TOKEN} from "../utils/RestContants";

const EditProfile = () => {

    //Bearer ;skdla;skdl;aksld;k;akdl;ksl;dkal;dkal;s
    return (
        <SocialLoginComponent
            text={"Connect"}
            googleUrl={GOOGLE_CONNECT_URL + '&' + ACCESS_TOKEN +'='+ localStorage.getItem(ACCESS_TOKEN)}
            facebookUrl={FACEBOOK_CONNECT_URL}
            githubUrl={GITHUB_CONNECT_URL}
        />
    )
}
export default EditProfile;
import {useState} from "react";

const CounterFunCom = () => {
    const [number, setNumber] = useState(0);

    const inc = () => {
        setNumber(number + 1)
    }
    const dec = () => {
        if (number > 0)
            setNumber(number - 1)
    }
    return (
        <div>
            <button onClick={inc}>+</button>
            <h4>{number}</h4>
            <button onClick={dec}>-</button>
        </div>
    )
}

export default CounterFunCom;
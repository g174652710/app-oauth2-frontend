import {useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";

const SignUpComponent = ({}) => {
    const [phoneNumber, setPhoneNumber] = useState('');
    const [password, setPassword] = useState('');
    const [prePassword, setPrePassword] = useState('');

    const register = () => {
        let req = {
            phoneNumber,
            password,
            prePassword
        };
        axios.post(
            'http://localhost:80/api/auth/sign-up',
            req
        )
            .then(res => {
                window.location.replace("/sign-in")
            })
            .catch(err => console.log(err))

        console.log(req)
    }


    return (
        <div>
            <h4>Register</h4>
            <label htmlFor="phoneNumber">Phone number</label>
            <br/>
            <input
                id="phoneNumber"
                type="text"
                placeholder="Enter phone number"
                onChange={(e) => setPhoneNumber(e.target.value)}
            />
            <br/>
            <label htmlFor="password">Password</label>
            <br/>
            <input
                id="password"
                type="password"
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
            />
            <br/>
            <label htmlFor="prePassword">PrePassword</label>
            <br/>
            <input
                id="prePassword"
                type="password"
                placeholder="Enter pre password"
                onChange={(e) => setPrePassword(e.target.value)}
            />
            <br/>
            <button onClick={register}>Register</button>
            <p>Already registered?</p>
            <Link to={"/sign-in"}>SignIn</Link>
        </div>
    )
}

export default SignUpComponent;
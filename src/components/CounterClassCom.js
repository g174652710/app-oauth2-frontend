import {Component} from "react";

class CounterClassCom extends Component {

    state = {
        number: 0,
        name:'Ketmon'
    }

    render() {
        const inc = () => {
            this.setState({number: this.state.number + 1})
        }

        const dec = () => {
            this.setState({number: this.state.number - 1})
        }

        return (
            <div>
                <button onClick={inc}>+</button>
                <h4>{this.state.number}</h4>
                <button onClick={dec}>-</button>
            </div>
        )
    }
}

export default CounterClassCom;
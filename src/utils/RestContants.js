export const ACCESS_TOKEN = "AccessToken";
export const REFRESH_TOKEN = "RefreshToken";
export const BASE_PATH = 'http://localhost/';
export const OAUTH2_REDIRECT_SIGN_SUCCESS_URI = 'http://localhost:3000/oauth2/redirect'
export const OAUTH2_REDIRECT_SIGN_FAIL_URI = 'http://localhost:3000/sign-in'
export const OAUTH2_REDIRECT_CONNECT_SUCCESS_URI = 'http://localhost:3000/cabinet/edit-profile'
export const OAUTH2_REDIRECT_CONNECT_FAIL_URI = 'http://localhost:3000/cabinet/edit-profile'
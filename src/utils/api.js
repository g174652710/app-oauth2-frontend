import {
    BASE_PATH, OAUTH2_REDIRECT_CONNECT_FAIL_URI,
    OAUTH2_REDIRECT_CONNECT_SUCCESS_URI,
    OAUTH2_REDIRECT_SIGN_FAIL_URI,
    OAUTH2_REDIRECT_SIGN_SUCCESS_URI
} from "./RestContants";

export const GOOGLE_SIGN_URL = BASE_PATH + 'oauth2/authorize/google?redirect_uri_sign_success=' + OAUTH2_REDIRECT_SIGN_SUCCESS_URI+"&redirect_uri_sign_fail="+ OAUTH2_REDIRECT_SIGN_FAIL_URI;
export const FACEBOOK_SIGN_URL = BASE_PATH + 'oauth2/authorize/facebook?redirect_uri_sign_success=' + OAUTH2_REDIRECT_SIGN_SUCCESS_URI+"&redirect_uri_sign_fail="+OAUTH2_REDIRECT_SIGN_FAIL_URI;
export const GITHUB_SIGN_URL = BASE_PATH + 'oauth2/authorize/github?redirect_uri_sign_success=' + OAUTH2_REDIRECT_SIGN_SUCCESS_URI+"&redirect_uri_sign_fail="+OAUTH2_REDIRECT_SIGN_FAIL_URI;

export const GOOGLE_CONNECT_URL = BASE_PATH + 'oauth2/authorize/google?redirect_uri_connect_success=' + OAUTH2_REDIRECT_CONNECT_SUCCESS_URI+"&redirect_uri_connect_fail="+ OAUTH2_REDIRECT_CONNECT_FAIL_URI;
export const FACEBOOK_CONNECT_URL = BASE_PATH + 'oauth2/authorize/facebook?redirect_uri_connect_success=' + OAUTH2_REDIRECT_CONNECT_SUCCESS_URI+"&redirect_uri_connect_fail="+OAUTH2_REDIRECT_CONNECT_FAIL_URI;
export const GITHUB_CONNECT_URL = BASE_PATH + 'oauth2/authorize/github?redirect_uri_connect_success=' + OAUTH2_REDIRECT_CONNECT_SUCCESS_URI+"&redirect_uri_connect_fail="+OAUTH2_REDIRECT_CONNECT_FAIL_URI;


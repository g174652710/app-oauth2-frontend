import React from 'react';
import ReactDOM from 'react-dom/client';
import MyRouter from './MyRouter';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <MyRouter/>
);

